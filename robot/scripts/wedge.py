from struct import unpack
from struct import *
import serial
import time
import rospy
from sensor_msgs.msg import NavSatFix
from std_msgs.msg import Int8MultiArray, Int8, String
import numpy as np

BAUD = 115200

DEBUG = True

BYTE_L = 108
BYTE_G = 103
BYTE_R = 114
BYTE_LINE = 124


ser = serial.Serial('/dev/ttyUSB0', BAUD)
print("Connected to: " + ser.portstr)


print(ser.baudrate)
print(ser.bytesize)
print(ser.parity)
print(ser.stopbits)
print(ser.xonxoff)
print(ser.rtscts)
print(ser.dsrdtr) 
print(ser.name)

# define gps publisher
gps_pub = rospy.Publisher('gps', NavSatFix, queue_size=10)

# define rtcm publisher
rtcm_pub = rospy.Publisher('rtcm', String, queue_size=10)
lora_pub = rospy.Publisher('lora', String, queue_size=10)

# define satelite publisher
sat_pub = rospy.Publisher('satellite', Int8, queue_size=10)

# define a string publisher
misc_pub = rospy.Publisher('misc', String, queue_size=10)

def parser(input):
    if DEBUG: print(type(list(input[0:])))
    
    # if this is an rtcm string, parse the rtcm message
    if input[0] == BYTE_R: 
        if DEBUG: print("rtcm", input[2:].decode('utf-8'))
        # publish the rtcm message
        rtcm_pub.publish(String(data=input[2:].decode('utf-8')))
    elif input[0] == BYTE_L: 
        if DEBUG: print("lora", input[2:].decode('utf-8'))
        # publish the rtcm message
        lora_pub.publish(String(data=input[2:].decode('utf-8')))
    elif input[0] == BYTE_G:
        parts = input.decode("utf-8").split("|")
        if DEBUG: print("gps", parts)
        c = [float(parts[4]), 0.0, 0.0, 0.0, float(parts[4]), 0.0, 0.0, 0.0, float(parts[4])]
        gps_msg = NavSatFix(header=None,
                            latitude=float(parts[1]),
                            longitude=float(parts[2]),
                            altitude=float(parts[3]),
                            position_covariance=c)
        gps_pub.publish(gps_msg)
        sat_pub.publish(Int8(data=int(parts[5])))
    else:
        if DEBUG: print(input)
        misc_pub.publish(String(data=input.hex()))

if __name__ == '__main__':
    rospy.init_node('talker', anonymous=True)
    # who needs rate limiting?
    # rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        arduino_data = ser.readline()
        if arduino_data:
            leng = len(arduino_data)
            if DEBUG: print(arduino_data)
            if arduino_data[0] in {BYTE_R, BYTE_G, BYTE_L} and arduino_data[1] == BYTE_LINE: parser(arduino_data[:leng-2])
    