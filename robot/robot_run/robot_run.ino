// #define SQUAWK
#define gps_debug
#define PRO_CPU	0
#define APP_CPU	1
#define BAUD_RATE 115200

// #include <LoRa.h>
#include "gps_things.hpp"


static SFE_UBLOX_GNSS GPS;


RTCM *string_builder = new RTCM(&GPS);

SemaphoreHandle_t xSemaphore = NULL;

long lastTime = 0; 
char buffer[RTCM_BUFFER];
int buf_len = 0;
bool updated = false;

void publish_gps(double lat, double lon, float alt, float acc, int sat){
    String printout = "g|" + String(lat, DEC) + "|" + String(lon, DEC) + "|" + String(alt, DEC) + "|" + String(acc, DEC) + "|" + String(sat, DEC);
    Serial.println(printout);
}



// process rtcm data coming from lora and manage that 
void on_receive(void *pvParameters){

    for(;;){
        #ifdef SQUAWK
        //Sprint("receiver: ");
        //Sprintln(xPortGetCoreID());
        #endif
        // check for new data from lora
        if(updated){
            // create a temporary buffer
            char data[buf_len];
            // copy the new data into the temporary buffer
            memcpy(data, buffer, buf_len);
            // push the temporary buffer to the processig sys
            string_builder->push_data(data);
            // signal that the data has been handled
            updated = false;
            
        }
        // vTaskDelete( NULL );
        // rate limit to 20ms
        vTaskDelay(20/portTICK_PERIOD_MS);
    }
}


// call back for lora as new bytes come in
void onReceive(int packetSize) {
    // read packet
    for (int i = 0; i < packetSize; i++){
        buffer[i] = (char)LoRa.read();
    }
    buf_len = packetSize;
    updated = true;
    // xTaskCreate(on_receive, "on_receive", 4096, NULL, 1, NULL);
}

void run_gps(){
    #ifdef SQUAWK
    //Sprint("gps: ");
    //Sprintln(xPortGetCoreID());
    #endif
    long start = millis();

    // //Sprintln("getting gps data");
    // First, let's collect the position data
    if( xSemaphore != NULL ){
        /* See if we can obtain the semaphore.  If the semaphore is not
        available wait 10 ticks to see if it becomes free. */
        if( xSemaphoreTake( xSemaphore, ( TickType_t ) 10 ) == pdTRUE ){
            /* We were able to obtain the semaphore and can now access the
            shared resource. */
            
            int32_t latitude = GPS.getHighResLatitude();
            int8_t latitudeHp = GPS.getHighResLatitudeHp();
            int32_t longitude = GPS.getHighResLongitude();
            int8_t longitudeHp = GPS.getHighResLongitudeHp();
            int32_t ellipsoid = GPS.getElipsoid();
            int8_t ellipsoidHp = GPS.getElipsoidHp();
            int32_t msl = GPS.getMeanSeaLevel();
            int8_t mslHp = GPS.getMeanSeaLevelHp();
            uint32_t accuracy = GPS.getHorizontalAccuracy();
            uint32_t time = GPS.getUnixEpoch();
            int satin = GPS.getSIV();
            xSemaphoreGive( xSemaphore );
    
            // Serial.println(time);
    
            // Defines storage for the lat and lon as double
            double d_lat; // latitude
            double d_lon; // longitude

            // Assemble the high precision latitude and longitude
            d_lat = ((double)latitude) / 10000000.0; // Convert latitude from degrees * 10^-7 to degrees
            d_lat += ((double)latitudeHp) / 1000000000.0; // Now add the high resolution component (degrees * 10^-9 )
            d_lon = ((double)longitude) / 10000000.0; // Convert longitude from degrees * 10^-7 to degrees
            d_lon += ((double)longitudeHp) / 1000000000.0; // Now add the high resolution component (degrees * 10^-9 )

            // Now define float storage for the heights and accuracy
            float f_ellipsoid;
            float f_msl;
            float f_accuracy;

            // Calculate the height above ellipsoid in mm * 10^-1
            f_ellipsoid = (ellipsoid * 10) + ellipsoidHp;
            // Now convert to m
            f_ellipsoid = f_ellipsoid / 10000.0; // Convert from mm * 10^-1 to m

            // Calculate the height above mean sea level in mm * 10^-1
            f_msl = (msl * 10) + mslHp;
            // Now convert to m
            f_msl = f_msl / 10000.0; // Convert from mm * 10^-1 to m

            // Convert the horizontal accuracy (mm * 10^-1) to a float
            f_accuracy = accuracy;
            // Now convert to m
            f_accuracy = f_accuracy / 10000.0; // Convert from mm * 10^-1 to m
            publish_gps(d_lat, d_lon, f_msl, f_accuracy, satin);

            #ifdef gps_debug
            Serial.print("Lat (deg): ");
            Serial.print(d_lat, 9);
            Serial.print(", Lon (deg): ");
            Serial.print(d_lon, 9);

            // Finally, do the printing
            Serial.print(", Ellipsoid (m): ");
            Serial.print(f_ellipsoid, 4); // Print the ellipsoid with 4 decimal places

            Serial.print(", Mean Sea Level (m): ");
            Serial.print(f_msl, 4); // Print the mean sea level with 4 decimal places

            Serial.print(", Accuracy (m): ");
            Serial.println(f_accuracy, 4); // Print the accuracy with 4 decimal places
            #endif
        }else{
            #ifdef gps_debug
            Serial.println("could not get semaphore");
            #endif
        }
    }
}

void gps_task(void *pvParameters){
    
    for(;;){
        run_gps();
        // vTaskDelay(20/portTICK_PERIOD_MS);
    }
}

void setup() {
    
    Serial.begin(BAUD_RATE, SERIAL_8N1);

    String starting = "starting...";
    String printout = "r|" + String(sizeof(starting)/sizeof(starting[0]), DEC) + "|" + starting;
    Serial.println(printout);

    xSemaphore = xSemaphoreCreateMutex();
    
    // out.println("setting up robot");

    // setup the robot gps with all of the robot gps things    
    robot_gps_setup(&GPS);

    //Sprintln("started gps");

    robot_lora_setup();
    LoRa.onReceive(onReceive);
   
    // run i2c on strong boi core
    // pin the gps query task to core 1
    // let it run async
    xTaskCreate(gps_task, "gps_task", 4096, NULL, 3, NULL);
    // check for lora bytes and run on what ever core
    xTaskCreate(on_receive, "on_receive", 2048, NULL, 2, NULL);
    // xTaskCreatePinnedToCore(push_data_to_zed, "send ready", 2048, NULL, 2, NULL, 1);
    // initialize the display 
    // ros_manager = new ROSManager();
    display_data(-1.0, -1.0, -1.0, -1.0, -1);
    //Sprintln("done setting up robot");

}

void loop() {
    #ifdef SQUAWK
    //Sprint("loop: ");
    //Sprintln(xPortGetCoreID());
    #endif

    digitalWrite(LED_BUILTIN, HIGH);
    vTaskDelay(100/ portTICK_PERIOD_MS);
    digitalWrite(LED_BUILTIN, LOW);
    vTaskDelay(100/ portTICK_PERIOD_MS);

}
