//#include "heltec.h"
#include <LoRa.h>
#include "gps_things.hpp"
#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

SFE_UBLOX_GNSS GPS;

RTCM string_builder(&GPS);

//    Heltec.display->clear();
//    Heltec.display->drawString(0, 0, "ROBOT");
//    Heltec.display->display();

long lastTime = 0; //Simple local timer. Limits amount if I2C traffic to u-blox module.


// on recieving rtcm data from lora push to ZED
void onReceive(int packetSize){
//    //Sprint(packetSize);
    // received a packet
//    //Sprint("Received packet: ");
    
    // read packet
    uint8_t dat = LoRa.read();
    string_builder.push_data(dat);
//    //Sprintln(dat, HEX);

}



void setup() {
    
    Serial.begin(115200);
    
    //Sprintln("setting up robot");
    
    robot_gps_setup(&GPS);
   
    LoRa.setPins(18, 14, 26);
    if (!LoRa.begin(915E6)) {
      //Sprintln("Starting LoRa failed!");
      while (1);
    }
 
    // register the receive callback
    LoRa.onReceive(onReceive);

    // put the radio into receive mode
    LoRa.receive();
    //Sprintln("done setting up robot");
}

void loop() {

    string_builder.send_ready();
    //Query module only every second. Doing it more often will just cause I2C traffic.
    //The module only responds when a new position is available
    if (millis() - lastTime > 1000)
    {
        //Sprintln("Querying module");
        lastTime = millis(); //Update the timer
        // getHighResLatitude: returns the latitude from HPPOSLLH as an int32_t in degrees * 10^-7
        // getHighResLatitudeHp: returns the high resolution component of latitude from HPPOSLLH as an int8_t in degrees * 10^-9
        // getHighResLongitude: returns the longitude from HPPOSLLH as an int32_t in degrees * 10^-7
        // getHighResLongitudeHp: returns the high resolution component of longitude from HPPOSLLH as an int8_t in degrees * 10^-9
        // getElipsoid: returns the height above ellipsoid as an int32_t in mm
        // getElipsoidHp: returns the high resolution component of the height above ellipsoid as an int8_t in mm * 10^-1
        // getMeanSeaLevel: returns the height above mean sea level as an int32_t in mm
        // getMeanSeaLevelHp: returns the high resolution component of the height above mean sea level as an int8_t in mm * 10^-1
        // getHorizontalAccuracy: returns the horizontal accuracy estimate from HPPOSLLH as an uint32_t in mm * 10^-1

        // First, let's collect the position data
        int32_t latitude = GPS.getHighResLatitude();
        int8_t latitudeHp = GPS.getHighResLatitudeHp();
        int32_t longitude = GPS.getHighResLongitude();
        int8_t longitudeHp = GPS.getHighResLongitudeHp();
        int32_t ellipsoid = GPS.getElipsoid();
        int8_t ellipsoidHp = GPS.getElipsoidHp();
        int32_t msl = GPS.getMeanSeaLevel();
        int8_t mslHp = GPS.getMeanSeaLevelHp();
        uint32_t accuracy = GPS.getHorizontalAccuracy();
    
        // Defines storage for the lat and lon as double
        double d_lat; // latitude
        double d_lon; // longitude
    
        // Assemble the high precision latitude and longitude
        d_lat = ((double)latitude) / 10000000.0; // Convert latitude from degrees * 10^-7 to degrees
        d_lat += ((double)latitudeHp) / 1000000000.0; // Now add the high resolution component (degrees * 10^-9 )
        d_lon = ((double)longitude) / 10000000.0; // Convert longitude from degrees * 10^-7 to degrees
        d_lon += ((double)longitudeHp) / 1000000000.0; // Now add the high resolution component (degrees * 10^-9 )

        //Sprint("Lat (deg): ");
        //Sprint(d_lat, 9);
        //Sprint(", Lon (deg): ");
        //Sprint(d_lon, 9);
    
        // Now define float storage for the heights and accuracy
        float f_ellipsoid;
        float f_msl;
        float f_accuracy;

         // Calculate the height above ellipsoid in mm * 10^-1
        f_ellipsoid = (ellipsoid * 10) + ellipsoidHp;
        // Now convert to m
        f_ellipsoid = f_ellipsoid / 10000.0; // Convert from mm * 10^-1 to m
    
        // Calculate the height above mean sea level in mm * 10^-1
        f_msl = (msl * 10) + mslHp;
        // Now convert to m
        f_msl = f_msl / 10000.0; // Convert from mm * 10^-1 to m
    
        // Convert the horizontal accuracy (mm * 10^-1) to a float
        f_accuracy = accuracy;
        // Now convert to m
        f_accuracy = f_accuracy / 10000.0; // Convert from mm * 10^-1 to m
    
        // Finally, do the printing
        //Sprint(", Ellipsoid (m): ");
        //Sprint(f_ellipsoid, 4); // Print the ellipsoid with 4 decimal places
    
        //Sprint(", Mean Sea Level (m): ");
        //Sprint(f_msl, 4); // Print the mean sea level with 4 decimal places
    
        //Sprint(", Accuracy (m): ");
        //Sprintln(f_accuracy, 4); // Print the accuracy with 4 decimal places

      

    }
    delay(100);
}
