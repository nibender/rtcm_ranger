#include "gps_things.hpp"

RTCM::RTCM(SFE_UBLOX_GNSS* G)
{
	GPS = G;
	head = 0;
	ready = false;
	data = (uint8_t*)calloc(RTCM_BUFFER, sizeof(uint8_t));
}

bool RTCM::get_ready()
{
	return ready;
}

void RTCM::free_data(){
	free(data);
	data = (uint8_t*)calloc(RTCM_BUFFER, sizeof(uint8_t));
}

void RTCM::push_data(char* s){
	int buf_len = strlen(s);
	publish_lora(s, buf_len);
	//Sprintln("push_data");
	uint8_t* temp = (uint8_t *)s;
	uint8_t c;
	for (int i = 0; i < sizeof(s)/sizeof(uint8_t); i++) {
		c = temp[i];

		if(c != 0xD3 && head == 0){
			//Sprintln("not processing data");
			continue;
		}else{
			// start new message and end current message
			if (c == 0xD3) {
				// send rtcm string to ublox
				ready = true;
				send_ready();
				//Sprintln("done sending");
				head = 0;
				//Sprintln("reset head again");
			}
			data[head] = c;
			head++;
		}
	}
}

bool RTCM::send_rtcm(uint8_t** arr, int len){
	//Sprintln("send_rtcm");
	//Sprintln(len);
	bool ret = GPS->pushRawData(*arr, len, false);
	publish_rtcm(*arr);
	free_data();
	return ret;
}

void RTCM::send_ready(){
	//Sprintln("=========Sending ready==========");
	if(ready == true && head > 0){
		// copy data to temporary array
		// uint8_t* t = (uint8_t*)calloc(head, sizeof(uint8_t));
		//Sprintln("setting up temp array");
		uint8_t t[head];
		for (int i = 0; i < head; i++) {
			t[i] = data[i];
			//Sprint(t[i], HEX);
			//Sprint(" ");
		}
		//Sprintln();
		bool ret = GPS->pushRawData(t, head, false);
		//Sprint("result of data push: ");
		
		//Sprintln(ret);
		free_data();
		// if (send_rtcm(&t, head) == false){
		// 	//Sprintln("Error sending rtcm to gps");
		// }
		//Sprintln("data freed");
		head = 0;
		// delete t;
		ready = false;
		//Sprintln("reset flag and head");
	}
}

void publish_rtcm(uint8_t *rtcm){
	String ss = "";
	for(int i = 0; i < strlen((char*)rtcm); i++){
		ss += String(rtcm[i], HEX);
	}
	String printout = "r|" + ss;
    	Serial.println(printout);
}

void base_gps_setup(SFE_UBLOX_GNSS* GPS)
{
	/*
	Serial1.begin(38400);
	GPS->begin(Serial1);
	GPS->setSerialRate(38400);
	//Sprintln("GNSS serial connected");
	*/
	Wire1.begin(SDA, SCL);
	Wire1.setClock(400000); // Increase I2C clock speed to 400kHz

	// Connect to the u-blox module using Wire port
	if (GPS->begin(Wire1) == false)
	{
		//Sprintln(F("u-blox GNSS not detected at default I2C address. Please check wiring. Freezing."));
		while (1)
			;
	}

	// Uncomment the next line if you want to reset your module back to the default settings with 1Hz navigation rate
	// GPS->factoryDefault(); delay(5000);

	GPS->setI2COutput(COM_TYPE_UBX);				   // Set the I2C port to output UBX only (turn off NMEA noise)
	GPS->saveConfigSelective(VAL_CFG_SUBSEC_IOPORT); // Save the communications port settings to flash and BBR


	bool response = true;
	response &= GPS->enableRTCMmessage(UBX_RTCM_1005, COM_PORT, 1); // Enable message 1005 to output through I2C port, message every second
	response &= GPS->enableRTCMmessage(UBX_RTCM_1074, COM_PORT, 1);
	response &= GPS->enableRTCMmessage(UBX_RTCM_1084, COM_PORT, 1);
	response &= GPS->enableRTCMmessage(UBX_RTCM_1094, COM_PORT, 1);
	response &= GPS->enableRTCMmessage(UBX_RTCM_1124, COM_PORT, 1);
	response &= GPS->enableRTCMmessage(UBX_RTCM_1230, COM_PORT, 10); // Enable message every 10 seconds

	// Use COM_PORT_UART1 for the above six messages to direct RTCM messages out UART1
	// COM_PORT_UART2, COM_PORT_USB, COM_PORT_SPI are also available
	// For example: response &= GPS->enableRTCMmessage(UBX_RTCM_1005, COM_PORT_UART1, 10);

	if (response == true)
	{
		//Sprintln(F("RTCM messages enabled"));
	}
	else
	{
		//Sprintln(F("RTCM failed to enable. Are you sure you have an ZED-F9P?"));
		while (1)
			; // Freeze
	}

	// Check if Survey is in Progress before initiating one
	//  From v2.0, the data from getSurveyStatus (UBX-NAV-SVIN) is returned in UBX_NAV_SVIN_t packetUBXNAVSVIN
	//  Please see u-blox_structs.h for the full definition of UBX_NAV_SVIN_t
	//  You can either read the data from packetUBXNAVSVIN directly
	//  or can use the helper functions: getSurveyInActive; getSurveyInValid; getSurveyInObservationTime; and getSurveyInMeanAccuracy
	response = GPS->getSurveyStatus(2000); // Query module for SVIN status with 2000ms timeout (request can take a long time)

	if (response == false) // Check if fresh data was received
	{
		//Sprintln(F("Failed to get Survey In status"));
		while (1)
			; // Freeze
	}

	if (GPS->getSurveyInActive() == true) // Use the helper function
	// if (GPS->packetUBXNAVSVIN->data.active > 0) // Or we could read active directly
	{
		//Sprint(F("Survey already in progress."));
	}
	else
	{
		// Start survey
		// The ZED-F9P is slightly different than the NEO-M8P. See the Integration manual 3.5.8 for more info.
		// response = GPS->enableSurveyMode(300, 2.000); //Enable Survey in on NEO-M8P, 300 seconds, 2.0m
		response = GPS->enableSurveyMode(60, 5.000); // Enable Survey in, 60 seconds, 5.0m
		if (response == false)
		{
			//Sprintln(F("Survey start failed. Freezing..."));
			while (1)
				;
		}
		//Sprintln(F("Survey started. This will run until 60s has passed and less than 5m accuracy is achieved."));
	}

	while (Serial.available())
		Serial.read(); // Clear buffer

	// Begin waiting for survey to complete
	while (GPS->getSurveyInValid() == false) // Call the helper function
	// while (GPS->packetUBXNAVSVIN->data.valid == 0) // Or we could read valid directly
	{

		// From v2.0, the data from getSurveyStatus (UBX-NAV-SVIN) is returned in UBX_NAV_SVIN_t packetUBXNAVSVIN
		// Please see u-blox_structs.h for the full definition of UBX_NAV_SVIN_t
		// You can either read the data from packetUBXNAVSVIN directly
		// or can use the helper functions: getSurveyInActive; getSurveyInValid; getSurveyInObservationTime; and getSurveyInMeanAccuracy
		response = GPS->getSurveyStatus(2000); // Query module for SVIN status with 2000ms timeout (req can take a long time)

		if (response == true) // Check if fresh data was received
		{
			//Sprint(F("Time elapsed: "));
			//Sprint((String)GPS->getSurveyInObservationTime()); // Call the helper function

			//Sprint(F(" Accuracy: "));
			//Sprint((String)GPS->getSurveyInMeanAccuracy()); // Call the helper function

			//Sprintln(F("m"));
		}
		else
		{
			//Sprintln(F("SVIN request failed"));
		}

		delay(1000);
	}
	//Sprintln(F("Survey valid!"));

	//Sprintln(F("Base survey complete! RTCM now broadcasting."));

	GPS->setI2COutput(COM_TYPE_UBX | COM_TYPE_RTCM3); // Set the I2C port to output UBX and RTCM sentences (not really an option, turns on NMEA as well)
}

void publish_lora(char *lora, int len){
	String ss = "";
	for(int i = 0; i < len; i++){
		ss += String(lora[i], HEX);
	}
	String printout = "l|" + ss;
	Serial.println(printout);
}

void robot_gps_setup(SFE_UBLOX_GNSS* GPS)
{
	Wire1.begin(SDA, SCL);
	Wire1.setClock(400000); // Increase I2C clock speed to 400kHz

	// GPS->enableDebugging(Serial);

	// Connect to the u-blox module using Wire port
	if (GPS->begin(Wire1) == false)
	{
		//Sprintln(F("u-blox GNSS not detected at default I2C address. Please check wiring. Freezing."));
		while (1)
			;
	}

	// Set the I2C port to output UBX only (turn off NMEA noise)
	GPS->setI2COutput(COM_TYPE_UBX);
	// Set output to 20 times a second
	GPS->setNavigationFrequency(10);

	// Get the update rate of this module
	byte rate = GPS->getNavigationFrequency();
	//Sprint("Current update rate: ");
	//Sprintln(rate);

	// GPS->saveConfiguration(); //Save the current settings to flash and BBR
}

void lora_setup(void)
{

	Heltec.begin(true /*Display Enable*/,
		true /*LoRa Enable*/,
		false /*Serial Enable, this fucks the serial to the computer*/,
		true /*PABOOST Enable*/,
		BAND /*long BAND*/);
}

void oled_setup(){
	Heltec.display->init();
	Heltec.display->flipScreenVertically();  
	Heltec.display->setFont(ArialMT_Plain_10);
}

void robot_lora_setup(void)
{
	lora_setup();
	LoRa.receive();
	
}

void base_lora_setup()
{
	lora_setup();

	//Sprintln("lora unlocked");
}

void display_data(double lat, double lon, float alt, float acc, int sat){
	// display the data
	Heltec.display->clear();
	Heltec.display->drawString(0, 0, "lat: " + (String)lat);
	Heltec.display->drawString(0, 10, "lon: " + (String)lon);
	Heltec.display->drawString(0, 20, "alt: " + (String)alt);
	Heltec.display->drawString(0, 30, "Acc: " + (String)acc);
	Heltec.display->drawString(0, 40, "sat: " + (String)sat);
	Heltec.display->display();
}
