#include <Arduino.h>
#include <HardwareSerial.h>
#include <Wire.h>
#include <SparkFun_u-blox_GNSS_Arduino_Library.h>
#include <heltec.h>
#include <ros.h>
#include <sensor_msgs/NavSatFix.h>
#include <std_msgs/UInt8MultiArray.h>
#include <std_msgs/Int8.h>


#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

#define SS 18	
#define RST_LoRa 14 
#define DIO0 26
#define ACCURACY 2.0

#define COM_PORT COM_PORT_I2C // COM_PORT_UART2, COM_PORT_USB, COM_PORT_SPI, COM_PORT_I2C

#define RTCM_BUFFER 512*4 // Size of the buffer for incoming RTCM messages
#define LORA_BUFFER 128 // Size of the buffer for outgoing LoRa messages

class RTCM{
	private:
		uint8_t* data;
		int head;
		SFE_UBLOX_GNSS* GPS;
		bool ready;
		void free_data();
	
	public:
		RTCM(SFE_UBLOX_GNSS* G);
		void push_data(char* s);
		bool send_rtcm(uint8_t **arr, int len);
		void send_ready();
		bool get_ready();
	
};

void publish_rtcm(uint8_t *rtcm);

// extern SafeSerial safeSerial;

void base_gps_setup(SFE_UBLOX_GNSS *GPS);

void publish_lora(char *lora, int len);

void robot_gps_setup(SFE_UBLOX_GNSS *GPS);

void robot_lora_setup(void);

void base_lora_setup(void);

void display_data(double lat, double lon, float alt, float acc, int sat);

// size_t print_this(const char *format, ...);

