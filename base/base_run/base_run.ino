#include "gps_things.hpp"

#define LORA_LED 17

static SFE_UBLOX_GNSS* GNSS_sys = new SFE_UBLOX_GNSS();

bool lock_lora = true;

void setup() {
  Serial.begin(115200);

  base_lora_setup(); // this must go first
  Serial.println("LORA setup done");
  // setup gps
  base_gps_setup(GNSS_sys);
  lock_lora = false;
}

void loop() {
  GNSS_sys->checkUblox(); // very important to get the data from the GPS
  // heartbeat routine
  digitalWrite(LED_BUILTIN, HIGH);
  vTaskDelay(125/ portTICK_PERIOD_MS);
  digitalWrite(LED_BUILTIN, LOW);
  vTaskDelay(125/ portTICK_PERIOD_MS);
}

void SFE_UBLOX_GNSS::processRTCM(uint8_t incoming) {
  static bool last_sync = HIGH;
  // make a buffer for a rtcm string 
  // this is so that we can send a few longer messages 
  static uint8_t rtcm_msg_buf[LORA_BUFFER];
  // keep track of the head of the buffer
  static int head = 0; 
  // lora is setup and ready to be used
  if(lock_lora == false){
    // put the incoming byte at the head in the array
    rtcm_msg_buf[head] = incoming;
    // move the head forward
    head++;
    // the buffer is full
    if(head == LORA_BUFFER){
      // reset head
      head = 0;
      LoRa.beginPacket();
      LoRa.setTxPower(14,RF_PACONFIG_PASELECT_PABOOST);
      // convert the buffer to a string in the janky way to treat it as a print 
      // lora.write(buffer, len) seems to be broken      
      LoRa.print((char*)rtcm_msg_buf);
      // async send buffer on lora
      LoRa.endPacket(true);
      digitalWrite(LORA_LED, HIGH);
      vTaskDelay(25/ portTICK_PERIOD_MS);
      digitalWrite(LORA_LED, LOW);
    }
  }
}
