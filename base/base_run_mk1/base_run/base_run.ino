#include "gps_things.hpp"

SFE_UBLOX_GNSS GNSS_sys;

bool lock_lora = true;

void setup() {
  Serial.begin(115200);

  base_lora_setup(&lock_lora);

  base_gps_setup(&GNSS_sys);

  lock_lora = false;
  
}

void loop() {
  GNSS_sys.checkUblox();
  float h = (float)GNSS_sys.getHorizontalAccuracy()/1000;
  float v = (float)GNSS_sys.getVerticalAccuracy()/1000;
  // Serial.println(F("accuracies (horz, vert (m)): %f, %f", h, v));
  Serial.print("accuracies (horz, vert (m)): ");
  Serial.print(h);
  Serial.print(", ");
  Serial.println(v);
  delay(1000);                       // wait for a second
}

void SFE_UBLOX_GNSS::processRTCM(uint8_t incoming){
#ifdef USE_SERIAL1
  //Push the RTCM data to Serial1
  Serial1.write(incoming);
#endif
  // Serial.println(incoming, HEX);

  if(lock_lora == false){
    // Serial.println(incoming, HEX);
    if(incoming == 0xD3){
      Serial.println("heart beat");
    }
    // send packet
    LoRa.beginPacket();
    LoRa.write(incoming);
    // put a true in here to run in async
    LoRa.endPacket();
    
  }
  
}
